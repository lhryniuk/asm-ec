# asm-ec [![Build Status](https://travis-ci.com/hryniuk/asm-ec.svg?token=hH6LPCLCyUFneJpS1zdy&branch=master)](https://travis-ci.com/hryniuk/asm-ec)

Assembler for the [The Educational Computer, Model 1](https://github.com/hryniuk/ec).

Converts assembly (TODO: provide docs) into absolute load file
(TODO: provide docs).

## Usage

```shell
usage: asm.py [-h] [--asm-file ASM_FILE]

Convert EC assembly to an ALF

optional arguments:
  -h, --help           show this help message and exit
  --asm-file ASM_FILE  assembly file's path
```